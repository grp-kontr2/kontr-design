# Policies

## Course
(TODO)
 * ViewCourse - student: Can see active and archived projects and course info (Will not see - new porjects)
 * ReadCourse - teacher: Can see all projects 
---
 * UpdateCourse - can update course info
 * SetNotesAccessToken

## Roles
 * AssignRoles
 * WriteRole (permissions, info)
 * ReadRole
 * AssignRoles

## Project
 * WriteProject (Update, Create)
 * DeleteProject ()
 * Archive

## Groups
 * Write
 * Read
 * Delete

## Submission
 * ReadAll
 * ReadOwn
 * ReadGroup (Optional)
 * ViewAllFiles
 * CreateSubmission
 * Resubmit


## Review
 * ReadAllReviews
 * ReadOwnReview
 * WriteOwnReview
 * WriteAllReview
 * Evaluate (IS API)






