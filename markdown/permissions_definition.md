* archive_projects: 
	* may archive projects
	* typically project owner
* assign_roles:
	* may manipulate user lists in roles in the course
	* may not change the roles in any other way
* create_submissions:
	* the user may create submissions under their own identity in the course
		* timing restrictions on projects apply (aubmissions allowed from-to)
	* typically students

* evaluate_submissions: 
	* the user may create evaluations for submissions in the course
		* only for submissions they may also write reviews for 
	* typically teacher / assistant
	* not implemented yet
* handle_notes_access_token: 
	* the user may add, remove and modify the access token used for communication with IS API
		* the access token is created with their identity
	* typically course owner
* read_all_submission_files: 
	* the user has access to all files in submissions in the course
		* test files
		* teacher Kontr results
	* typically teachers/owner
* read_groups:
	* the user may see full information about all groups in the course
		* name, members
	* if not true, the user only sees names of groups 
* read_projects: 
	* the user may see full information about all projects in the course
		* name, state, configuration, (submissions)
	* if not true, the user sees only project name, state and start&end date (?)
* read_reviews_all:
	* the user may access all reviews of all submissions in the course they can read
* read_reviews_groups: 
	* the user may access reviews for submissions they can read only in the groups they are in
* read_reviews_own: 
	* the user may access only reviews associated with their submissions if they can read them
* read_roles:
	* the user may see full information about roles in the course 
		* name, description, (users), permissions
	* if not true, the user only sees name, description
* read_submissions_all:
	* the user may access all submissions in the course
* read_submissions_groups:
	* the user may access all submissions in the groups they are part of
	* typically seminar teacher
* read_submissions_own:
	* the user may access only submissions they authored
	* typically student
* resubmit_submissions:
	* the user may resubmit an existing submission under the original owner's identity
	* a note should be set for the new submission, explaining why the resubmit was called
	* not bound by time restrictions on project
	* typically owner / teacher
* update_course:
	* the user may update course information (name, codename)
	* the user may create, delete roles, groups, projects
	* the user may update roles, groups, projects
* view_course_full: 
	* the user may access full info about the course
		* all groups, roles and projects
* view_course_limited: 
	* the user may only see a subset of all course information
		* only groups and roles they are part of, and projects in the ACTIVE state
* write_groups: 
	* the user may update groups in the course
		* members of groups
		* name
* write_projects:
	* the user may update projects in the course 
		* name
		* configuration
* write_reviews_all:
	* the user may write, update and delete reviews in all submissions
* write_reviews_group:
	* the user may write, update and delete reviews in all submissions in groups they are part of
* write_reviews_own:
	* the user may write, update and delete reviews in submissions they authored
	* typically students
* write_roles:
	* the user may update roles in the course
		* name
		* members
