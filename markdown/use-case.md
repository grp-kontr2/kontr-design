# USE CASES

## Student can do this
### Auth
 * Login
 * Logout
 * Register (system account)
 * Edit his account
 * Link with Gitlab
 * Change/set password

### Course
 * Read

### Project
 * Read

### Submissions
 * Read (only his)
    * Without test files
    * Only student result view
 * Create
 * Review
    * Comment
    * Comment others (feature)

## Course ADMIN and Teacher can do this

### Course
 * Read
 * update
 * archive
 * set IS API token
 * Import from IS
 * Copy Course


 ### Project
 * CRUD
 * Copy (clone) project with/without configurations. (feature)
 * Templates? (feature)
 * Schedule - deadline, Set state
    * Submission available from and to
    * Viewable from and to
    * Archive at
 * CONFIG - Edit submission config, KONTR
    * ON_SUBMIT script
        * TEST FILES
        * Evaluation engine
        * Work with SUBMSSION parameters
        * Submission evaluation requirements - FEATURES - (for scheduler)
    * ON_RESULT script
        * Work with SUBMISSION params.
        * Process the Engine result
        * Notify users based on their roles and submssion params.
        * Post submit actions - cleanup? (optional)
    * SUBMISSION PARAMETERS definition

### Group
 * CRUD
 * Import from IS
 * Copy from course
 * Shuffle (feature c++)

### Role
 * CRUD
    * Permissions
 * Assign roles


### Submission
 * Resubmit a student's submission
 * Create and view
 * Review submission
    * Evaluate
    * Comment
 * Compare submissions (diff - FEATURE)

## Sys admin can do this
Superset of teacher

### Course
 * Create
 * Delete

### Submissions
 * Delete

### Users
 * CRUD




<!--stackedit_data:
eyJoaXN0b3J5IjpbNTIxOTU5NThdfQ==
-->