# Portal API

## General
 * Api prefix: `/api/v1.0/`


## Course
 * `[GET]     /courses`      - List all
 * `[POST]    /courses`      - Create
 * `[GET]     /courses/{id}` - Read
 * `[PUT]     /courses/{id}` - Update
 * `[DELETE]  /courses/{id}` - Delete
 * `[PUT]     /courses/{id}/notes_access_token` - Create/Update access token for IS API
 * `[GET]     /courses/{id}/notes_access_token` - Read access token for IS API


## Role
 * `[GET]     /courses/{cid}/roles` - List
 * `[POST]    /courses/{cid}/roles` - Create
 * `[GET]     /courses/{cid}/roles/{id}` - Read
 * `[PUT]     /courses/{cid}/roles/{id}` - Update
 * `[DELETE]  /courses/{cid}/roles/{id}` - Delete

### Role users
 * `[GET]     /courses/{cid}/roles/{id}/users` - List users
 * `[PUT]     /courses/{cid}/roles/{id}/users` - Change users in role
 * `[POST]    /courses/{cid}/roles/{id}/users/{uid}` - add user to role
 * `[DELETE]  /courses/{cid}/roles/{id}/users/{uid}` - remove user from role

### Role permissions
 * `[GET]     /courses/{cid}/roles/{id}/permissions` - List permissions
 * `[PUT]     /courses/{cid}/roles/{id}/permissions` - Update permissions

## Group
 * `[GET]     /courses/{cid}/groups` - List
 * `[POST]    /courses/{cid}/groups` - Create
 * `[GET]     /courses/{cid}/groups/{id}` - Read
 * `[PUT]     /courses/{cid}/groups/{id}` - Update
 * `[DELETE]  /courses/{cid}/groups/{id}` - Delete

### Group users
 * `[GET]     /courses/{cid}/groups/{id}/users` - List users
    * `role=<role_selector>`  Select users by role
 * `[PUT]     /courses/{cid}/groups/{id}/users` - Change users in group
 * `[POST]    /courses/{cid}/groups/{id}/users/{uid}` - add user to group
 * `[DELETE]  /courses/{cid}/groups/{id}/users/{uid}` - remove user from group

## Project
 * `[GET]     /courses/{cid}/projects` - List
 * `[POST]    /courses/{cid}/projects` - Create
 * `[GET]     /courses/{cid}/projects/{id}` - Read
 * `[PUT]     /courses/{cid}/projects/{id}` - Update
 * `[DELETE]  /courses/{cid}/projects/{id}` - Delete

### Project config
 * `[GET]     /courses/{cid}/projects/{id}/config` - Read
 * `[PUT]     /courses/{cid}/projects/{id}/config` - Update

### Project test files
 * `[GET]     /courses/{cid/projects/{id}/files` - Gets test files (feature)

### Project submissions
 * `[GET]     /courses/{cid}/projects/{id}/submissions` - Read all submissions for project
    * `user=<user_selector>` - filters submissions by user
 * `[POST]     /courses/{cid}/projects/{id}/submissions` - Create submission

## Submission (needs course id)
 * `[GET]   /submissions/{id}` - Reads submission
 * `[POST]  /submissions/{id}/state` - Updates state
 * `[GET]   /submissions/{id}/user` - Gets user of the submission
 * `[GET]   /submissions/{id}/groups` - Gets all groups for the user
 * `[GET]   /submissions/{id}/files` - Gets zip file with submission content
 * `[GET]   /submissions/{id}/result` - Gets zip file with result
 * `[POST]  /submissions/{id}/result` - Uploads zip with result


### Notifications API
 * `[PUT]   /notifications/{type}`
    * `type`: `email`, `notes`, `webhook`



