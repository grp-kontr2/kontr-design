## Permissions list:

* viewCourseLimited 
* viewCourseFull
* updateCourse 
* handleNotesAccessToken

* assignRoles
* writeRoles
* readRoles

* writeProjects
* readProjects
* archiveProjects
* createSubmissions
* resubmitSubmissions

* writeGroups
* readGroups

* readAllSubmissions
* readOwnSubmissions
* readGroupSubmissions
* viewAllSubmissionFiles

* readAllReviews
* readGroupReviews
* readOwnReviews
* writeAllReviews
* writeGroupReviews
* writeOwnReviews
* evaluateSubmissions