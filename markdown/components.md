# Components


## High level
 * Portal
 * Storage
 * DB
 * Submissions Processing
 * Kontr Application


## Portal
 * Identity provider
    * Users
    * Gitlab integration
 * Entities management
    * Course
    * Project
    * Role
    * Group
 * Access management
    * Based on policies
    * Inputs: Operation, Entity, User
    * Outputs: ALLOW, DENY
 * Notifications
    * Emails
    * UI notifications (feature)
    * WebHooks (feature)
    * IS API (separate API)
    * API to be able to send Notification
 * Storage management
    * Save the submission
        * From gitlab
        * From zip
        * Other (feature)
    * API to access the storage (submissions)
    * Compress the submissions
 * Submissions management
 * Submissions scheduler
 * Review
 * Web UI

## Storage
 * Save
 * READ
 * Checkout
 * Compress
 * Filter (blacklist, whitelist)
 * Access the submissions
 * Save the review


## Submission processing
 * API -> Python DSL library
    * ON_SUBMIT
    * ON_RESULT
 * Access to storage, DB, portal
 * Result processor


## KONTR
 * Execution Scheduler
 * Worker - physichal machine
 * Executor - What will be executing - Docker, process on the machine
 * KTDK



