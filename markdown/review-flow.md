# Review flow

## Prerequisites
 * Student submission in the system already exists
 * Teacher can provide review
 * Student can comment the review

## Flow

### Teacher
* Will have available the students submissions for the specific project in specific course
* Will be on page: Course -> Project -> Submissions -> Student's submision
* Will navigate to source codes
* Will be able to create new review and add the comments to the submited files
* Will be able to write point to IS Notes
    * text area which is by default disabled
    * can enable the text area
    * can update the notes in the IS
* Will save the review

### Portal
* Will save the review and comments
* Will update the IS notes
* Will send an email to student that his submission has been revied by -- teacher

### Student
* Will receive an email/notification
* Will take a look at review
* Will comment on points that are not clear
* Will create a new submission




