# Submission flow

## FLOW
### Student
 * Will go to a submission page for the project in his course
 * Will click on the create new submission button
 * Will fill the required informatios for the submissions
    * URL or ZIP archive with files
    * TAGS
    * Type of submission - TAGS
 * Will click on button to submit the form

### Portal
 * Will receive the submission from a student
 * Will forward submission to the submission scheduler

### Submission scheduler/manager
 * Will check whether student can submit
 * Will create a submission object and fills it with provided data
    * Configuration
    * Informations about the student
    * Execution date and time (When to execute)
 * Will send the provided files/URL to Storage module to checkout files

### Storage module
 * Will checkout student files from git/url/zip
 * Will notify the submission manager that files has been succesuly checkouted

### Submission manager
 * Will update submission state (checkouted)

### Submission manager (When execution time!)
 * Will send information to the submission processing engine to execute tne submission

### Submission processing
 * Will receive an information from the submission scheduler/manager
 * Will start executing the ON_SUBMIT script
 * Will decide what to do with an Submission - KONTR/Gitlab

### Kontr Application
 * Will receive the required informations from the Submission processing unit
 * Will decide on which worker the submission will be scheduled - based on features

### Worker
 * Will download the test_files and student's submission from storage
 * Will create workspace directory
 * Will create the container
    * Mount the base directory with student files, test files and workspace
    * Disable the network for the container `docker network disable`
    * Start the executor of the submission with provided arguments

### Docker image executor
 * Will receive the configuration
 * Will start the execution of the KTDK
 * Will save results of the KTDK

### Worker
 * Will store the workspace results to the storage
 * Will notify the Kontr application that submission ended

### Kontr Application
 * Will forward the notification to the submission processing

### Submission processing
 * Will receive the notification
 * Will start ON_RESULT script
    * It has access to: storage, result, notification, and submission configuration
 * Will process the result and creates the messages and outputs
    * Creates test result for the student and teacher (different informations)
    * Creates an email/notes notification that will send to portal
    * Creates request to modify the submission state

### Portal
 * Will update the submission state
 * Will send an email to the specified users/roles/groups
 * Will update the notes for the student


