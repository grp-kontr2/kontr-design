# PORTAL API

## User
* [GET] /users/ : list all users (only sysadmin)
	- Query:
		- group=gid : filter by group (teacher)
		- course=cid : filter by course (teacher)
* [POST] /users/ : create a new user (only sysadmin (component - IS import?))

* [GET] /users/<uid> : view info about one user
	- if uid == auth.current_user: allow, else only for sysadmin; same for derived paths
* [PUT] /users/<uid> : update user; different schema validations/format
	- non-admin: name, password_hash, only self
	- admin : + uco, email, username, is_admin, for all
* [DELETE] /users/<uid> : delete a user from the database
	- only sysadmin
	
* [GET] /users/<uid>/submissions : list all submissions (paginated); needs submission id, project id, course id; (maybe timing info)
	- **Query:**
		- project=pid : filter by project
		- course=cid : filter by course, group by course projects
	- clickable, redirects to /submissions/<id>

* [GET] /users/<uid>/roles : list user's roles 
	- **Query:**
		- course=cid : filter by course
	- same helper function as authorization
	- maybe inaccessible through UI? (what for?)

* [GET] /users/<uid>/courses : list courses the user has at least one role in (is part of)
	- show only recent/active (non-archived)?

* [GET] /users/<uid>/groups : list groups the user is a part of
	- group, filter by course
	
* [GET] /user/<uid>/reviews : list reviews associated with the user
	- reviews the user contributed to
	- reviews associated with the user's submissions: accessible through the user's submissions
	
	
## Submission
* [GET] /submissions/<sid> : view submission; 
	- authorize: submission author / teacher with appropriate rights / sysadmin
	- navigation info (project, course)
	- submission info - timing, configuration
	- link to review (not my work...?)
	- response:
	```json
		{
			"id":string
			"note":string
			"state":string
			"scheduled_for":unspecified/time
			"parameters":"{
				"key1":value1,
				... (project specific)
			}" -- JSON string
			"project":{
				"id":project_id, string
				"name":string
				"course":{course_id, string}
			}
			"user":user_id, string
			
		}
	```
* [GET] /submissions/<sid>/files : get files zip or a navigable structure
	- HEADER:: Accept : zip/json (... Accept-Encoding: compress, gzip; see https://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html)
		- json: returns a tree representation with the files; shown as clickable links in UI, redirect to (below)
	[GET] /submissions/<sid>/files/sources?file="src/main.c" : internal
	[GET] /submissions/<sid>/files/results?file="student-result.c" : internal
	- Response:
	```json
	{
		"files" = {
			"sources":[
				"src/main.c"
				"src/input.txt"
			],
			"results": [
				"student-result.c",
				"teacher-result.c"
			]
		}
	}
	```
* [GET] /submissions/<sid>/sources : returns a zip with student's files from storage
	- allowed only in state READY
	- internal (Kontr)

* [GET] /submissions/<sid>/test_files : returns a zip with test files from storage
	- allowed only in state READY
	- internal (Kontr)
	- always returns the most recent version
	
* [GET] /submissions/<sid>/state : read submission's state
* [PUT] /submissions/<sid>/state : update submission state
	- careful with allowed transitions
	- CANCELLED/ERRORED can't be updated, forbid result upload too
	
* [PUT] /submissions/<sid>/result : upload zip with submission result
	- forbidden on CANCELLED/ERRORED submissions
* [POST] /submissions/<sid>/resubmit : copies the <sid> submission and marks it for processing
	- information that the new submission is a resubmit is added to its note and the source submission's note
	- sources are *copied* from the source submission in storage, not downloaded again; check for new test_files as usual (will be executed with newest version)
	- Request:
	```json
		{
			"message":"Manual resubmit because of a Kontr failure"
		}
	```

* [GET] /submissions/<sid>/review : read all review items in the context of their associated files (not my work, I'll just list them)
* [POST] /submissions/<sid>/review : add some review items; maybe PUT, (PATCH)
	- request: could drop the outer object and review_items key; could be expanded to allow review edits
	```json
		{
			"review_items":[
				{
					"file":"src/main.c",
					"line":10,
					"content":"This is wrong!"
				},
				{
					"file":"src.main.c",
					"line":15,
					"content":"Nice job!"
				}
			]
		}
	```

* [DELETE] /submissions/<sid> : delete a submission and all associated reviews
	- only sysadmin

## Course
* [GET] /courses : list all courses
	- authorization: only sysadmin?
* [POST] /courses : create a new course (only sysadmin)

* [GET] /courses/<cid> : view course info
* [PUT] /courses/<cid> : update course info
	- name, codename
	
* [DELETE] /courses/<cid> : delete a course, all associated groups, projects, roles
	- only sysadmin
* [POST] /courses/<cid>/archive : archives a course, all associated groups, projects, roles
	- requires a new codename
	- request:
	```json
		{
			"archive_as":"PB161_2017"
		}
	```
* [PUT] /courses/<cid>/import : extends a course's configuration using a copy of another course's config
	- request:
	```json
		{
			"source_course":"PB161_2017"
			"config":{
				"roles":"bare"/"with_users"
				"groups":"bare"/"with_users"
				"projects":"bare"
			}
		}
	```
	OR (for import from IS) -- not implemented
	```json
		{
			"course":{JSON course representation}
		}
	```
	- does not change id, name or codename
* [PUT] /courses/<cid>/notes_access_token - Create/Update access token for IS API
* [GET] /courses/<cid>/notes_access_token - Read access token for IS API
* [PUT] /courses/<cid>/config - updated cource configuration
	- request:
	```json
		{
		
		}
	```
 
### Role
* [GET] /courses/<cid>/roles : list the course's roles
	- only teachers (see_roles permisison?)
* [POST] /courses/<cid>/roles : create a new role
* [GET] /courses/<cid>/roles/<rid> : view a role
* [PUT] /courses/<cid>/roles/<rid> : update a role
	- only name, description
* [DELETE] /courses/<cid>/roles/<rid> : delete a role

* [GET] /courses/<cid>/roles/<rid>/users : list users in a role
* [PUT] /courses/<cid>/roles/<rid>/users : change list of users in a role
	- use only user ID's, not whole entities
	- request:
	```json
	{
		"add": ["uid1","uid2", "uid3"],
		"delete": ["uid4"]
	}
	```
* [PUT] /courses/<cid>/roles/<rid>/users/<uid> : add user to role
* [DELETE] /courses/<cid>/roles/<rid>/users/<uid> : remove user from role

* [GET] /courses/<cid>/roles/<rid>/permissions : list permissions
* [PUT] /courses/<cid>/roles/<rid>/permissions : update role permissions
	- overwrites current
	- subset of all allowed permissions; omitted ones are not changed

### Group
* [GET] /courses/<cid>/groups : list the course's groups
	- only teachers (see_roles permisison?)
* [POST] /courses/<cid>/groups : create a new group
* [GET] /courses/<cid>/groups/<gid> : view a group
* [PUT] /courses/<cid>/groups/<gid> : update a group
	- only name
* [PUT] /courses/<cid>/groups/<gid>/import : copies a group from another course
	- request:
	```json
		{
			"source_course":"course_id"
			"source_group":"group_id"
			"with_users":"bare"/"with_users"
		}
	```
	OR (for import from IS) -- not implemented
	```json
		{
			"content":{JSON group representation}
		}
	```
* [DELETE] /courses/<cid>/groups/<gid> : delete a group

* [GET] /courses/<cid>/groups/<gid>/users : list users in a group
	- **Query:**
		- role=rid - filer by role
	
* [PUT] /courses/<cid>/groups/<gid>/users : change list of users in a group
	- replace
	- use only user ID's not, whole entities
	- request:
	```json
	{
		"add": ["uid1","uid2", "uid3"],
		"remove": []
	}
	```
* [PUT] /courses/<cid>/groups/<gid>/users/<uid> : add single user to group
* [DELETE] /courses/<cid>/groups/<gid>/users/<uid> : remove a user from role

### Project
 * `[GET] /courses/<cid>/projects` : list projects of a course
	- students see all active, teachers all --- needs new permissions
 * `[POST] /courses/<cid>/projects` : create a new project
 
 * `[GET] /courses/<cid>/projects/<pid>` : view a project
	- name
	- timing (from, to); teachers also see archive_from? (too much bother)
 * `[PUT] /courses/<cid>/projects/<pid>` : update a project
	- only name, state; sysadmin all?
 * `[DELETE] /courses/<cid>/projects/<pid>` : delete a project

 * `[GET] /courses/<cid>/projects/<pid>/config` : view project configuration
 * `[PUT] /courses/<cid>/projects/<pid>/config` : edit project configuration
	- replaces 

 (* `[GET] /courses/<cid>/projects/<pid>/files` : Gets test files (feature))

 * `[GET] /courses/<cid>/projects/<pid>/submissions` : Read all submissions for project
    * `user=<user_selector>` - filters submissions by user
 * `[POST] /courses/<cid>/projects/<pid>/submissions` : Create submission




Submission creation format: project id, user id passed in URL; file_params are mandatory and set, project_params are based on project configuration
{
	"file_params":{
		"source":{
			"type":"git"/"zip"
			"url":url, string,
			"branch":string,
			"checkout":string
		}
	},
	"project_params":{
		"key1":"value1",
		...
	}
}


submisison for storage: id, dictionary
{
	"source":{
		"type":"git"/"zip"
		"path" : string OR
		"url":url, string,
		"branch":string,
		"checkout":string
	},
	"whitelist":str
}















